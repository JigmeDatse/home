# The Grid
The Grid is a fork of the Matrix protocol with a different set of core values and a true community management.  
It aims to be care more for privacy and much more friendly to self-hosted infrastructures.

## Status
We are currently putting it all together before official kickoff.

See the [Sunrise](https://gitlab.com/thegridprotocol/home/milestones/1) milestone for the current progress.

## Documents
The following bootstrap documents explain what The Grid is in more details, how it came to be, differences with Matrix,
how the protocol will be managed by the non-profit and how documentation should take place.
- [Project Overview](docs/overview.md)
- [Governing Body](docs/governing-body.md)
- [Documentation Principles](docs/doc-principles.md)

## Contact
Visit us on:
- Matrix
  - [#thegrid:kamax.io](https://matrix.to/#/#thegrid:kamax.io)
  - [#kamax-thegrid:t2bot.io](https://matrix.to/#/#kamax-thegrid:t2bot.io)

- Telegram: [@thegrid_matrix](https://t.me/thegrid_matrix)
